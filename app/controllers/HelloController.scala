package controllers

import akka.http.scaladsl.model.HttpHeader.ParsingResult.Ok
import javax.inject.Inject
import javax.inject.Singleton
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.mvc.{AbstractController, Action, AnyContent, Controller, ControllerComponents, Request}

@Singleton
class SumNumberController @Inject() (cc:ControllerComponents) extends AbstractController(cc) {
  def getNum(num1: Option[Int], num2: Option[Int]) =
    Action { implicit request: Request[AnyContent] =>
      val sum: Int = num1.getOrElse(0) + num2.getOrElse(0)
      Ok(s"${sum}")
    }
}

class LanguageController @Inject()(val messagesApi: MessagesApi) extends Controller with I18nSupport{
  def get(name: Option[String]) =
    Action { implicit request =>
      Ok {
        name
          .map(s => Messages("hello", s))
          .getOrElse(Messages("noQuery"))
      }
    }
}
